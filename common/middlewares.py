import pytz
from django.conf import settings
from django.utils import (
	timezone, 
	translation
)



class ActivateTimezoneMiddleware(object):

	def trytz(self,name):

		try :
			tzname = pytz.timezone(name)
			print("timezone is ok")
		except pytz.exceptions.UnknownTimeZoneError:
			tzname = None

		return tzname


	def __init__(self,get_response):

		self.get_response = get_response

	def __call__(self,request):



		if request.META.get('HTTP_TIME_ZONE') is None :

			response = self.get_response(request)
			return response
		
		tzname = self.trytz(name=request.META.get('HTTP_TIME_ZONE'))

		if tzname:
			timezone.activate(tzname)

		response = self.get_response(request)

		return response


class AdminTranslateMiddleware(object):

	def __init__(self,get_response):

		self.get_response = get_response

	def __call__(self,request):


		if  request.path.startswith('/admin/'):
			translation.activate(settings.LANGUAGE_CODE)

		response = self.get_response(request)

		return response
