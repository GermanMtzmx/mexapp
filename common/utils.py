import os
import binascii
from django.conf import settings
from django.utils import timezone
from django.core.mail import EmailMessage
from django.utils.translation import ugettext_lazy as _
from django.utils.deconstruct import deconstructible


class CommonMixin:

    @classmethod
    def randomName(cls,size=6):
        return binascii.hexlify(os.urandom(size)).decode()

    @classmethod
    def uploadpath(cls):

        today  = timezone.now()

        abspath = "%d/%d/%d" % (today.year,today.month,today.day)


        if not os.path.exists(os.path.join(settings.MEDIA_ROOT,abspath)):
            os.makedirs(os.path.join(settings.MEDIA_ROOT,abspath))

        return abspath  


    def getObjectOrNone(self, model=None, *args, **kwargs):

        try:
            entity = model.objects.get(*args, **kwargs)
        except model.DoesNotExist:

            entity = None

        return entity

    def returnSerializerErrors(self, dict={}):
        missingFields = []

        if not bool(dict):
            return False

        for k, v in dict.items():

            missingFields.append(k)

        fields = ', '.join(missingFields)

        return "%s %s" % (_("The following fields are required : "), fields)

    def sendEmail(self, subject, htmlContent, sender, to):

        email = EmailMessage(
            subject,
            htmlContent,
            sender,
            to=to
        )

        email.content_subtype = 'html'

        try :
            
            email.send(fail_silently=False)
            
            return True

        except email.SMTPException:

            return False

    @deconstructible
    @classmethod
    def uploadAndRename(cls,instance,filename):
        ext = filename.split('.')[-1]
        renamedfile = '{}/{}.{}'.format(cls.uploadpath(),cls.randomName(6),ext)
        return renamedfile  