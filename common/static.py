# import yout models here


# import your catalogs here from common.catalogs


FILL_DATA = [

	# Structure data is : 


	# {
	# 	'name':"Name of catalog",
	# 	'model':ModelOfCatalog,
	# 	'data':DATA_CATALOG,
	# 	'fill':True


	# },	

]



def updateOrCreateObject(name=None,model=None,data={},fill=False):

	if model is not None and fill:

		for row in data:
			try :
				obj = model.objects.get(**row)

				for k,v in row.items():
					setattr(obj,k,v)
					print(name," updating: ",k )
				obj.save()

			except model.DoesNotExist:
				print ("saving data into ----> ",name)
				obj = model(**row)
			obj.save()
		return
				

def main():
	for info in FILL_DATA:
		updateOrCreateObject(**info)

	return