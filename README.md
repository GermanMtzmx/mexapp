# Mexapp

Mexapp is an api to get postal code info by the states, municipalities, and localities in Mexico 


### Install linux dependencies

```shell
sudo apt-get install libpq-dev python-dev python3-dev postgresql postgresql-contrib python-virtualenv python-pip

```

### Virtualenv configuration

Create a new virtualenv

```shell
virtualenv -p /usr/bin/python3  virtualenvname

```

Activate the virtualenv 

```shell

source /path/to/yourVirtualenv/virtualenvName/bin/activate

```

Install dependencies into virtualenv

under the project folder `mexapp` install requirements with pip (activate the virtualenv before install dependences)

```shell
pip install -r requirements.txt
```

### Collect statics files

collect static files  with `python manage.py collectstatic`

### Set a new environment 

Into folder `mexapp` edit the file `envs.py`


Create a new env class by example : 


Sqlite configuration database

```python

class MyClassEnv(Env):
	
	DATABASES = {

		'default': {
			'ENGINE':'django.db.backends.sqlite3',
			'NAME': os.path.join(BASE_DIR,'db.sqlite3'),
		},

	}


```

Postgresql configuration database

```python
class MyClassEnv(Env):
	
	DATABASES = {
		'default': {
			'ENGINE':'django.db.backends.postgresql_psycopg2',
			'NAME':'dbname',
			'USER':'dbuser',
			'PASSWORD':'dbpassword',
			'HOST':'localhost',
			'PORT':'5432'
		},
	}

```


Then add your env class into `envs` dict

```python
	
	envs = {
		'yourHostname':MyClassEnv,
	}

```

### Set custom settings for your env class

MEDIA DOMAIN - To serve correctly media set your custom media domain in your env class by example

```python
MEDIA_DOMAIN = "http://59a65efd.ngrok.io/"

```


EMAIL SETTINGS - Overwrite  email settings in your env class


```python

	EMAIL_HOST = 'YourSmtpHost.com'
	EMAIL_PORT = 587
	EMAIL_USE_TLS = True
	EMAIL_HOST_USER = 'rouruser@mail.com'
	EMAIL_HOST_PASSWORD = 'usserPassword'


```


### Apply migrations

```shell
python manage.py migrate

```

### Create a super user for django admin

```shell
python manage.py createsuperuser

```

#### Run the project

Run  it with testing server

```shell
python manage.py runserver 0.0.0.0:(port)

```


Run it with gunicorn 

```shell

gunicorn --workers (num of workers) --bind 0.0.0.0:(port) --reload --daemon mexapp.wsgi

```

### Fill initial data (deprecated)

This method will fill all whole data aviable in mexico country , coult take a long time to end be patient


```python
from states.fill_data import main
main()
```

### Fill initial data with fixtures

This method will load data from `mexapp.json` fixture which contains all necesary info for api works 

```python
python manage.py loaddata mexapp.json
```


### Endpoints 

Postman endpoint collection aviable [Here ...](https://www.getpostman.com/collections/3858425ff380e690556c)


### Notes 

We use the [Sepomex database info](www.sepomex.gob.mx/lservicios/servicios/CodigoPostal_Exportar.aspx) to make it work with django api