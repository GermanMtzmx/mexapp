from django.contrib import admin

# Register your models here.

from accounts.models import (
	User,
	RecoveryToken
)


class UserAdmin(admin.ModelAdmin):

	list_display = (
		'name',
		'lastName',
		'created',
	)

class RecoveryTokenAdmin(admin.ModelAdmin):
	list_display = (
		'key',
		'expired',
		'expiration',
	)


admin.site.register(User,UserAdmin)
admin.site.register(RecoveryToken,RecoveryTokenAdmin)