from django.conf.urls import url 
#your urls here

from accounts.views import (
	SignupAPIView,
	SigninAPIView,
	ProfileAPIView,
	ForgotPasswordAPIView,
	ResetPasswordAPIView,


)

urlpatterns = [
	#url('^pattern$',yourviewHere)
	url(r'^accounts/signup$',SignupAPIView.as_view()),
	url(r'^accounts/signin$',SigninAPIView.as_view()),
	url(r'^accounts/me$',ProfileAPIView.as_view()),

	url(r'^accounts/password/forgot$',ForgotPasswordAPIView.as_view()),
	url(r'^accounts/password/reset$',ResetPasswordAPIView.as_view()),

]

