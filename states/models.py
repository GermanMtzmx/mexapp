from django.db import models
from django.utils.translation import ugettext_lazy as _


# Create your models here.

class State(models.Model):

	name = models.CharField(max_length=200,null=False,blank=False)

	def __str__(self):

		return self.name

	class Meta:
		verbose_name = _("State")
		verbose_name_plural = _("States")

class Municipality(models.Model):

	state = models.ForeignKey('states.State',null=False,blank=False)
	name = models.CharField(max_length=200,null=False,blank=False)


	def __str__(self):

		return self.name 

	class Meta:
		 verbose_name = _("Municipality")
		 verbose_name_plural = _("Municipalities")


class PostalCode(models.Model):

	state = models.ForeignKey('states.State',null=False,blank=False)
	municipality = models.ForeignKey('states.Municipality',null=False,blank=False)
	code = models.CharField(max_length=10,null=False,blank=False)

	def __str__(self):

		return self.code


	class Meta:
		verbose_name = _("Postal code")
		verbose_name_plural = _("Postal codes")


class KindOfLocality(models.Model):

	name = models.CharField(max_length=200,null=False,blank=False)

	def __str__(self):
		return self.name 

	class Meta:
		verbose_name = _("Kind of locality")
		verbose_name_plural = _("Kind of localities")


class Locality(models.Model):

	kind = models.ForeignKey('states.KindOfLocality',null=False,blank=False)
	#municipality = models.ForeignKey('states.Municipality',null=False,blank=False)
	postal_code = models.ForeignKey('states.PostalCode',null=False,blank=False)
	name = models.CharField(max_length=200,null=False,blank=False)

	def __str__(self):

		return self.name

	class Meta:
		verbose_name = _("Locality")
		verbose_name_plural = _("Localities")
