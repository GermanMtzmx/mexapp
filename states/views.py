from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from accounts.permissions import AllowAny
from django.utils.translation import ugettext_lazy as _


from states.models import (

	State,
	Municipality,
	PostalCode,
	KindOfLocality,
	Locality,
)


from states.serializers import (

	StateSerializer,
	MunicipalitiesSerializer,
	LocalitySerializer,
	PostalCodeLocalitySerializer,
	PostalCodeSerializer,
	PostalCodeStateSerializer,
	PostalCodeMunicipalitySerializer,
)

class StatesAPIView(APIView):


	permission_classes = (AllowAny,)


	def get(self,request):
		states = State.objects.all()
		serializer = StateSerializer(states,many=True)
		return Response(data=serializer.data,status=status.HTTP_200_OK)



class MunicipalitiesByStateAPIView(APIView):

	permission_classes = (AllowAny,)


	def get(self,request,state_id):

		municipalities = Municipality.objects.filter(state__id=state_id)

		serializer = MunicipalitiesSerializer(municipalities,many=True)

		return Response(data=serializer.data,status=status.HTTP_200_OK)


class PostalCodesByStateAPIView(APIView):

	permission_classes = (AllowAny,)

	def get(self,request,state_id):

		postal_codes = PostalCode.objects.filter(
			state__id = state_id
		)

		serializer  = PostalCodeStateSerializer(postal_codes,many=True)

		return Response(data=serializer.data,status=status.HTTP_200_OK)



class LocalitiesByMunicipalityAPIView(APIView):

	permission_classes = (AllowAny,)


	def get(self,request,municipality_id):


		localities = Locality.objects.filter(postal_code__municipality__id=municipality_id)

		serializer = LocalitySerializer(localities,many=True)

		return Response(data=serializer.data,status=status.HTTP_200_OK)


class PostalCodesByMunicipalityAPIView(APIView):

	permission_classes = (AllowAny,)

	def get(self,request,municipality_id):

		postal_codes = PostalCode.objects.filter(municipality__id=municipality_id)

		serializer = PostalCodeMunicipalitySerializer(postal_codes,many=True).data

		return Response(data=serializer,status=status.HTTP_200_OK)


class InfoByPostalCodeAPIView(APIView):

	permission_classes = (AllowAny,)

	def get(self,request,postal_code):
		
		code = PostalCode.objects.get(code=postal_code)
		localities = Locality.objects.filter(postal_code__code=postal_code)

		response_data  = PostalCodeSerializer(code,many=False).data
		response_data['localities'] = PostalCodeLocalitySerializer(localities,many=True).data

		return Response(data=response_data,status=status.HTTP_200_OK)



		







