from django.contrib import admin
from states.models import (

	State,
	Municipality,
	KindOfLocality,
	Locality,
	PostalCode
)

# Register your models here.

class PostalCodeAdmin(admin.ModelAdmin):

	list_display = ('code','state','municipality')

class StateAdmin(admin.ModelAdmin):

	list_display = ('name',)

class MunicipalityAdmin(admin.ModelAdmin):
	list_display = ('state','name')

class KindOfLocalityAdmin(admin.ModelAdmin):
	list_display = ('name',)

class LocalityAdmin(admin.ModelAdmin):

	list_display = ('postal_code','kind','name')


admin.site.register(State,StateAdmin)
admin.site.register(Municipality,MunicipalityAdmin)
admin.site.register(KindOfLocality,KindOfLocalityAdmin)
admin.site.register(Locality,LocalityAdmin)
admin.site.register(PostalCode,PostalCodeAdmin)
