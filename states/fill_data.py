import os
from states.models import (

    State,
    Municipality,
    KindOfLocality,
    Locality,
    PostalCode,
)
from django.conf import settings
from common.utils import CommonMixin


class Fill(CommonMixin):
    source = os.path.join(settings.BASE_DIR, 'states/data/data.txt')

    def states(self):
        with open(self.source, encoding="utf-8") as register:

            for line in register:
                name = line.split('|')[4]
                state = self.getObjectOrNone(model=State, name=name)

                if state is None:
                    print("Saving new state --> ", name)
                    State.objects.create(name=name)

    def municipalities(self):
        with open(self.source, encoding="utf-8") as register:
            for line in register:
                state = self.getObjectOrNone(State, name=line.split('|')[4])
                name = line.split('|')[3]
                municipality = self.getObjectOrNone(Municipality, name=name)

                if municipality is None:
                    Municipality.objects.create(name=name, state=state)
                    print("Saving new municipality --> ", name)

    def postalCodes(self):

        with open(self.source, encoding="utf-8") as register:
            for line in register:
                state = self.getObjectOrNone(State, name=line.split('|')[4])
                municipality = self.getObjectOrNone(Municipality,
                                                    name=line.split('|')[3])

                code = self.getObjectOrNone(
                    PostalCode, state=state,
                    municipality=municipality,
                    code=line.split('|')[0])
                if code is None:
                    PostalCode.objects.create(
                        state=state,
                        municipality=municipality,
                        code=line.split('|')[0]
                    )

                    print("Saving postal code  --> ", line.split('|')[0])

    def kindLocalities(self):

        with open(self.source, encoding="utf-8") as register:

            for line in register:
                name = line.split('|')[2]
                kind = self.getObjectOrNone(KindOfLocality, name=name)

                if kind is None:
                    KindOfLocality.objects.create(
                        name=name
                    )

                    print("Saving kind of locality --> ", name)

    def localities(self):

        with open(self.source, encoding="utf-8") as register:

            for line in register:

                kind = self.getObjectOrNone(
                    KindOfLocality, name=line.split('|')[2])
                code = self.getObjectOrNone(
                    PostalCode, code=line.split('|')[0])

                locality = self.getObjectOrNone(
                    Locality, kind=kind,
                    postal_code=code, name=line.split('|')[1])

                if locality is None:

                    Locality.objects.create(
                        name=line.split('|')[1],
                        kind=kind,
                        postal_code=code
                    )

                    print("Saving new locality --> ", line.split('|')[1])

    def all(self):

        print("######### Filling states ##########")
        self.states()
        print("######### Filling municipalities ##########")
        self.municipalities()
        print("######### Filling postal codes ##########")
        self.postalCodes()
        print("######### Filling kind of localities ##########")
        self.kindLocalities()
        print("######### Filling localities ##########")
        self.localities()


def main():
    fill = Fill()
    fill.all()
