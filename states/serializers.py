from rest_framework import serializers

from states.models import (

	State,
	Municipality,
	PostalCode,
	KindOfLocality,
	Locality,
)



class StateSerializer(serializers.ModelSerializer):

	class Meta:

		model = State
		fields = '__all__'

class MunicipalitiesSerializer(serializers.ModelSerializer):

	class Meta:
		model = Municipality
		fields = '__all__'



class KindOfLocalitySerializer(serializers.ModelSerializer):

	class Meta:
		model = KindOfLocality
		fields = '__all__'


class LocalitySerializer(serializers.ModelSerializer):

	kind = KindOfLocalitySerializer(many=False)

	class Meta:
		model = Locality
		fields = '__all__'



class PostalCodeLocalitySerializer(serializers.ModelSerializer):

	class Meta:

		model = Locality
		exclude = (
			'postal_code',
			'kind',
		)



class PostalCodeSerializer(serializers.ModelSerializer):

	state = StateSerializer(many=False)
	municipality = MunicipalitiesSerializer(many=False)

	class Meta:
		model = PostalCode
		fields = '__all__'



class PostalCodeStateSerializer(serializers.ModelSerializer):

	municipality = MunicipalitiesSerializer(many=False)

	class Meta:
		model = PostalCode
		exclude = ('state',)


class PostalCodeMunicipalitySerializer(serializers.ModelSerializer):

	class Meta:
		model = PostalCode
		exclude = ('state','municipality')
