from django.conf.urls import url


from states.views import (
	StatesAPIView,
	MunicipalitiesByStateAPIView,
	LocalitiesByMunicipalityAPIView,
	InfoByPostalCodeAPIView,
	PostalCodesByStateAPIView,
	PostalCodesByMunicipalityAPIView,
)


urlpatterns = [
	
	url(r'^state/all$',StatesAPIView.as_view()),
	url(r'^state/(?P<state_id>[0-9]{1,})/municipalities$',MunicipalitiesByStateAPIView.as_view()),
	url(r'^state/(?P<state_id>[0-9]{1,})/postalcodes$',PostalCodesByStateAPIView.as_view()),

	url(r'municipality/(?P<municipality_id>[0-9]{1,})/localities$',LocalitiesByMunicipalityAPIView.as_view()),
	url(r'municipality/(?P<municipality_id>[0-9]{1,})/postalcodes$',PostalCodesByMunicipalityAPIView.as_view()),


	url(r'^postalcodeinfo/(?P<postal_code>[0-9]{1,10})$',InfoByPostalCodeAPIView.as_view()),

]