
from django.conf.urls import (
    url,
    include,
)
from django.contrib import admin

from django.views.static import serve
from django.conf import  settings 


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    #serve static files
    url(r'^static/(?P<path>.*)$',serve,{'document_root': settings.STATIC_ROOT}),

    #apps urls
    url(r'^api/v1/',include('accounts.urls')),
    url(r'^api/v1/',include('common.urls')),
    url(r'^api/v1/',include('states.urls')),

    #serve media files
    url(r'^api/v1/serve/(?P<path>.*)$',serve,{'document_root':settings.MEDIA_ROOT})
]
